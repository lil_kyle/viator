
lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'viator/version'

Gem::Specification.new do |spec|
  spec.name          = 'viator'
  spec.version       = Viator::VERSION
  spec.authors       = ['Kyle Church']
  spec.email         = ['kyle@kylechur.ch']
  spec.summary       = 'Simple Error Handler'
  spec.description   = 'Error handling without exceptions or monads'
  spec.homepage      = 'https://bitbucket.org/surdegg/viator/'
  spec.license       = 'MIT'
  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
