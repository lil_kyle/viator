RSpec.describe Viator do
  let(:vinstance) { described_class.new }

  it 'has a version number' do
    expect(Viator::VERSION).not_to be nil
  end

  it 'has a nil value on creation' do
    expect(vinstance.value).to be(nil)
  end

  it 'has an empty error list on creation' do
    expect(vinstance.errors.length).to be(0)
  end

  it 'succeeds on creation' do
    expect(vinstance.success?).to be(true)
  end

  it 'does not fail on creation' do
    expect(vinstance.failure?).to be(false)
  end

  it 'reports its value when succeeding' do
    vinstance.value = 3
    expect(vinstance.value).to be(3)
  end

  it 'fails after a report' do
    vinstance.report 'there has been a problem'
    expect(vinstance.success?).to be(false)
  end

  it 'gives its count as the length of its errors' do
    vinstance.report 'this is problem number one'
    vinstance.report 'this is problem number two'
    vinstance.report 'this is problem number three'
    expect(vinstance.count).to be(3)
  end

  it 'gives the same count as errors.length' do
    vinstance.report 'this is problem number one'
    vinstance.report 'this is problem number two'
    vinstance.report 'this is problem number three'
    expect(vinstance.errors.length).to be(vinstance.count)
  end

  it 'does not report its value if failing' do
    vinstance.value = 3
    vinstance.report 'there has been a problem'
    expect(vinstance.value).to be(nil)
  end

  it 'reports a failing value if that option is set' do
    vinstance = described_class.new(hide_value: false)
    vinstance.value = 3
    vinstance.report 'there has been a problem'
    expect(vinstance.value).to be(3)
  end

  it 'succeeds after a reset' do
    vinstance.report 'there has been a problem'
    vinstance.reset
    expect(vinstance.success?).to be(true)
  end

  it 'has a value if that option is triggered on creation' do
    temp_viator = described_class.new(value: 'foo')
    expect(temp_viator.value).to eq('foo')
  end
end
