require 'viator/version'

# Easy and Clean Error Handler
class Viator
  attr_writer   :value
  attr_reader   :errors
  attr_accessor :hide_value

  Errors = Class.new(Array)

  def initialize(options = {})
    @errors     = Errors.new
    @value      = options[:value]
    @hide_value = (options[:hide_value] != false)
  end

  def success?
    @errors.length.zero?
  end

  def failure?
    !success?
  end

  def report(str)
    @errors.push(str)
  end

  def count
    @errors.length
  end

  def reset
    @errors = []
  end

  def value
    @value unless failure? && @hide_value
  end
end
